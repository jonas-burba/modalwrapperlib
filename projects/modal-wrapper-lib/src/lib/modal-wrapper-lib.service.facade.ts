import { Injectable, Injector, SkipSelf } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ModalWrapperService } from './modal-wrapper-lib.service';

type ModalWrapperServiceType = {
  [K in keyof ModalWrapperService]: ModalWrapperService[K];
};

interface ModalWrapperServiceInterface extends ModalWrapperServiceType {} // tslint:disable-line: no-empty-interface

@Injectable()
export class ModalWrapperServiceFacade implements ModalWrapperServiceInterface {

  constructor(
    private readonly injector: Injector,
    @SkipSelf() private readonly modalWrapperService: ModalWrapperService
  ) { }

  public hideAll(): void {
    this.modalWrapperService.hideAll();
  }

  public show(content: any, config?: ModalOptions, injector?: Injector): BsModalRef {
    const viewInjector = injector;
    let customInjector: Injector | undefined;
    if (viewInjector) {
      const moduleInjector = this.injector;
      const activatedRoute = viewInjector.get(ActivatedRoute);
      customInjector = Injector.create({
        providers: [
          { provide: ActivatedRoute, useValue: activatedRoute }
        ],
        parent: moduleInjector
      });
    }
    return this.modalWrapperService.show(content, config, customInjector || this.injector);
  }
}

import { InjectionToken } from '@angular/core';
import { ModalOptions } from 'ngx-bootstrap/modal';
export const MODAL_OPTIONS = new InjectionToken<ModalOptions>('MODAL_OPTIONS');
export const defaultModalOptions: Required<ModalOptions> = {
  backdrop: true,
  keyboard: true,
  focus: true,
  show: false,
  ignoreBackdropClick: false,
  class: '',
  animated: true,
  initialState: {}
};

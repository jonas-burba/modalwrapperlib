import { InjectionToken, ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalBackdropComponent, ModalDirective, ModalModule as NgxBootstrapModalModule, ModalOptions } from 'ngx-bootstrap/modal';
import { LazyModalContenWrapperComponent } from './lazy-modal-content-wrapper/lazy-modal-content-wrapper.component';
import { ModalWrapperService } from './modal-wrapper-lib.service';
import { ModalWrapperServiceFacade } from './modal-wrapper-lib.service.facade';
import { defaultModalOptions, MODAL_OPTIONS } from './di/modalOptions';

export const MODAL_OPTIONS_INPUT = new InjectionToken<ModalOptions>('MODAL_OPTIONS_INPUT');

export const modalOptionsFactory = (modalOptions: ModalOptions) => ({...defaultModalOptions, ...modalOptions});

/** @dynamic */
@NgModule({
  declarations: [
    LazyModalContenWrapperComponent,
  ],
  imports: [
    CommonModule,
    NgxBootstrapModalModule
  ],
  exports: [
    ModalBackdropComponent,
    ModalDirective,
    // any other NgxBootstrapModalModule export
  ],
  entryComponents: [
    LazyModalContenWrapperComponent,
  ]
})
/** @dynamic */
export class ModalWrapperModule {

  static forRoot(modalOptions: ModalOptions): ModuleWithProviders {
    return {
      ngModule: ModalWrapperModule,
      providers: [
        { provide: MODAL_OPTIONS_INPUT, useValue: modalOptions },
        { provide: MODAL_OPTIONS, useFactory: modalOptionsFactory, deps: [MODAL_OPTIONS_INPUT] },
        ...NgxBootstrapModalModule.forRoot().providers
      ]
    };
  }

  static forChild(): ModuleWithProviders {
    return {
      ngModule: ModalWrapperModule,
      providers: [
        { provide: ModalWrapperService, useClass: ModalWrapperServiceFacade}
      ]
    };
  }
}

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  Injector,
  Type,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

@Component({
  selector: 'lazy-modal-content-wrapper', // tslint:disable-line: component-selector
  template: `<ng-template #host></ng-template>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LazyModalContenWrapperComponent {

  @ViewChild('host', { read: ViewContainerRef}) private host: ViewContainerRef | undefined;

  public initialState?: Object; // tslint:disable-line: ban-types

  constructor(
    // BsModalRef instance members are defined after modal content component is constructed within `BsModalService.show()` method
    private readonly modalOptions: ModalOptions,
    private readonly bsModalRef: BsModalRef,
    private readonly cdr: ChangeDetectorRef,
  ) { }

  public show<T>(contentComponent: Type<T>, injector: Injector) {
    const newInjector = Injector.create({
      providers: [
        { provide: ModalOptions, useValue: this.modalOptions },
        { provide: BsModalRef, useValue: this.bsModalRef }
      ],
      parent: injector
    });
    const componentFactoryResolver: ComponentFactoryResolver = injector.get(ComponentFactoryResolver); // tslint:disable-line: deprecation
    const componentFactory = componentFactoryResolver.resolveComponentFactory(contentComponent);
    const componentRef = this.host!.createComponent(componentFactory, undefined, newInjector); // tslint:disable-line: no-non-null-assertion
    // or
    // const componentRef = componentFactory.create(injector);
    // this.vcr.insert(componentRef.hostView);
    Object.assign(componentRef.instance, this.initialState);
    this.cdr.markForCheck();
    componentRef.changeDetectorRef.detectChanges();
  }
}

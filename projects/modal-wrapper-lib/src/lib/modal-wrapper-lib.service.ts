import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  ElementRef,
  Inject,
  Injectable,
  Injector,
  Renderer2,
  RendererFactory2,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Subscription } from 'rxjs';
import { BsModalService, BsModalRef, ModalOptions, ModalBackdropComponent } from 'ngx-bootstrap/modal';
import { LazyModalContenWrapperComponent } from './lazy-modal-content-wrapper/lazy-modal-content-wrapper.component';
import { MODAL_OPTIONS } from './di/modalOptions';

const modalContainerSelector = 'modal-container';

/** @dynamic */
@Injectable({
  providedIn: 'root'
})
/** @dynamic */
export class ModalWrapperService {

  private backdropComponentRef: ComponentRef<ModalBackdropComponent>;
  private backdropElementRef: ElementRef<HTMLElement>;
  private readonly renderer: Renderer2;
  private readonly bsModalRefs: BsModalRef[] = [];
  private moveBackdropSubscription: Subscription;

  constructor(
    private readonly bsModalService: BsModalService,
    private readonly componentFactoryResolver: ComponentFactoryResolver,
    private readonly rendererFactory: RendererFactory2,
    @Inject(DOCUMENT) private readonly document: Document,
    private readonly applicationRef: ApplicationRef,
    @Inject(MODAL_OPTIONS) private readonly modalOptionsGlobal: Required<ModalOptions>,
  ) {
    this.document = document as Document;
    this.modalOptionsGlobal = modalOptionsGlobal as Required<ModalOptions>;
    this.renderer = this.rendererFactory.createRenderer(undefined, null); // tslint:disable-line: no-null-keyword
  }

  public hideAll() {
    for (let i = this.bsModalRefs.length - 1; i > -1; i--) {
      this.bsModalRefs[i].hide();
    }
  }

  public show(content: any, config?: ModalOptions, injector?: Injector): BsModalRef {
    return injector
      ? this.showIndirectly(content, config, injector)
      : this.showDirectly(content, config);
  }

  private showIndirectly(content: any, config: ModalOptions | undefined, injector: Injector): BsModalRef {
    // https://github.com/microsoft/TypeScript/issues/26235
    const configOrEmpty = config || {};
    const { initialState, ...rest } = configOrEmpty;
    const lazyConfig = { ...rest, initialState: { initialState } };
    const lazyBsModalRef = this.showDirectly(LazyModalContenWrapperComponent, lazyConfig);
    const lazyInstance: LazyModalContenWrapperComponent = lazyBsModalRef.content;
    lazyInstance.show(content, injector);
    return lazyBsModalRef;
  }

  private showDirectly(content: any, config: ModalOptions | undefined): BsModalRef {
    // using returned merged config in case 3rd party default config values change
    const { config: modalOptions, backdrop, backdropClick, keyboard } = this.overrideConfig(config);
    // BsModalRef instance members are defined after modal content component is constructed within `BsModalService.show()` method
    const bsModalRef = this.bsModalService.show(content, modalOptions);
    const removeEventListeners = this.addEventListeners(backdropClick, keyboard, () => bsModalRef.hide());
    this.addBackdrop(backdrop);
    const hideRef = bsModalRef.hide;
    bsModalRef.hide = () => { hideRef(); removeEventListeners(); this.bsModalRefs.pop(); };
    this.bsModalRefs.push(bsModalRef);
    return bsModalRef;
  }

  private overrideConfig(config?: ModalOptions) {
    const mergedConfig = { ...this.modalOptionsGlobal, ...config };
    const backdrop = !!mergedConfig.backdrop;
    const backdropStatic = mergedConfig.backdrop === 'static';
    const ignoreBackdropClick = mergedConfig.ignoreBackdropClick;
    const backdropClick = !(backdropStatic || ignoreBackdropClick);
    const keyboard = !!mergedConfig.keyboard;
    return {
      config: { ...mergedConfig, backdrop: false, ignoreBackdropClick: true, keyboard: false },
      backdrop,
      backdropClick,
      keyboard,
    };
  }

  private addEventListeners(backdropClick: boolean, keyboard: boolean, hideCallback: (() => void)): () => void {
    const unlisteners: (() => void)[] = [];
    if (hideCallback) {
      if (backdropClick) {
        const modalContainer = this.modalContainers()[this.bsModalService.getModalsCount() - 1];
        const onClick = this.onClickFactory(hideCallback, modalContainer);
        const unlisten = this.renderer.listen(modalContainer, 'mousedown', onClick);
        unlisteners.push(unlisten);
      }
      if (keyboard) {
        const onEsc = this.onEscFactory(this.bsModalService.getModalsCount(), hideCallback);
        const unlisten = this.renderer.listen(window, 'keydown.esc', onEsc);
        unlisteners.push(unlisten);
      }
    }
    return () => unlisteners.forEach(v => v());
  }

  private onEscFactory = (level: number, hideCallback: () => void) => (event: KeyboardEvent) => {
    if (event.keyCode === 27 || event.key === 'Escape') { // tslint:disable-line: deprecation
      event.preventDefault();
    }

    if (level === this.bsModalService.getModalsCount()) {
      hideCallback();
    }
  }
  private onClickFactory = (hideCallback: () => void, container: Element) => (event: MouseEvent) => {
    if (event.target === container) {
      hideCallback();
    }
  }

  private addBackdrop(backdrop: boolean) {
    if (backdrop) {
      if (!this.backdropElementRef) {
        this.createBackdrop();
      } else {
        this.moveBackdrop();
      }
    }
  }

  private createBackdrop() {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(ModalBackdropComponent);
    this.backdropComponentRef = componentFactory.create(undefined as unknown as Injector); // TODO: Investigate
    this.applicationRef.attachView(this.backdropComponentRef.hostView);
    this.insertBefore(this.backdropComponentRef.location.nativeElement);
    this.backdropElementRef = this.backdropComponentRef.location;
    this.backdropComponentRef.instance.isShown = true;
    this.moveBackdropSubscription = this.bsModalService.onHidden.subscribe(this.moveBackdrop.bind(this));
  }

  private moveBackdrop() {
    if (this.bsModalService.getModalsCount() === 0) {
      this.moveBackdropSubscription.unsubscribe();
      this.backdropComponentRef.destroy();
      this.backdropElementRef.nativeElement.remove();
      this.backdropElementRef = undefined as unknown as ElementRef<HTMLElement>;
      this.backdropComponentRef = undefined as unknown as ComponentRef<ModalBackdropComponent>;
    } else {
      this.insertBefore(this.backdropElementRef.nativeElement);
    }
  }

  private insertBefore<T extends Node>(newChild: T): T | undefined {
    const container = 'body';
    if (container === 'body' && typeof document !== 'undefined') {
      const modalContainers = this.modalContainers();
      const lastContainer = modalContainers[modalContainers.length - 1]!; // tslint:disable-line: no-non-null-assertion
      return this.document.body.insertBefore(newChild, lastContainer);
      // lastContainer.insertAdjacentElement('beforebegin',this.backdropElementRef.nativeElement)
    }
  }

  private modalContainers(): NodeListOf<Element> {
    return this.document.querySelectorAll(modalContainerSelector);
  }

}

/*
 * Public API Surface of modal-wrapper-lib
 */

export * from 'ngx-bootstrap/modal';
export * from './lib/modal-wrapper-lib.service';
export { ModalWrapperModule } from './lib/modal-wrapper-lib.module';

# ModalWrapperLib

A split of a modal wrapper for https://valor-software.com/ngx-bootstrap/#/modals from monolith project to separate lib to fix some issues and/or add customization:

- add lazy load compatibility
  - provide child injector from lazy loaded module for modal instance
    - either imperatively (calling show method with injector)
    - or declaratively (using LazyModalContenWrapperComponent within template)
    - allows to inject correct ActivatedRoute instance to modal instance created from lazy loaded module
- add modal backdrop support for nested modals
- fix modal hiding on mouse event that started within modal, but ended on background/backdrop

## Code scaffolding

Run `ng generate component component-name --project ModalWrapperLib` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project ModalWrapperLib`.
> Note: Don't forget to add `--project ModalWrapperLib` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build ModalWrapperLib` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build ModalWrapperLib`, go to the dist folder `cd dist/modal-wrapper-lib` and run `npm publish`.

## Running unit tests

Run `ng test ModalWrapperLib` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
